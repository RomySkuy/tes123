# negara = set(['brazil', 'rusia', 'indonesia'])

# print ('indonesia' in negara)
# negara2 = negara.copy()
# negara2.add('korea')

# print (negara2.issuperset(negara))

# negara.remove('rusia')

# print (negara & negara2)
# print (negara2.intersection(negara))

# daftar_belanja = ['apel', 'mangga', 'wortel', 'pisang']
# print ('assignment biasa')
# daftar_saya = daftar_belanja

# del daftar_belanja[0]

# print ('daftar belanja:', daftar_belanja)
# print ('daftar saya:', daftar_saya)

# print ('copy obyek daftar belanja menggunakan slice [:]')
# daftar_saya = daftar_belanja[:] # membuat copy

# del daftar_saya[0]

# print ('daftar belanja:', daftar_belanja)
# print ('daftar saya:', daftar_saya)

nama = 'Indonesia'

if nama.lower().startswith('ind'):
    print ('Nama diawal dengan "ind"')
if 'ne' in nama:
    print ('Nama berisi string "ne"')
if nama.find('do') != -1:
    print ('Nama berisi string "done"')