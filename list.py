daftar_siswa = ['Romi','Bono','Agus','Castelo','Momon']
print ('Jumlah siswa di kelas berjumlah %s' % len(daftar_siswa))

print ('Nama siswa di kelas A:')
for nama in daftar_siswa:
    print (nama)

print ('Ada siswa baru masuk')
daftar_siswa.append('Ricky')
print ('Daftar siswa sekarang :', daftar_siswa)

print ('Mengurutkan nama siswa')
daftar_siswa.sort()
print (daftar_siswa)

print ('Urutan siswa nomer 1 DO', daftar_siswa[0])
siswa_1 = daftar_siswa[0]

del daftar_siswa[0]

print ('Siswa ini DO', siswa_1)
print ('Daftar siswa sekarang:', daftar_siswa)
